package com.aliceresponde.itunesstore.Entities;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import java.io.Serializable;

/**
 * Created by alice on 4/4/16.
 */
public class App  extends RealmObject implements Serializable{

  @PrimaryKey
  private String name;

  private String image_url;
  private String category;
  private String price;
  private String summary;
  private String link_url;
  private String rights;

  public App() {
  }

  public App(String name, String image_url, String category, String price, String summary,
      String link_url, String rights) {
    this.name = name;
    this.image_url = image_url;
    this.category = category;
    this.price = price;
    this.summary = summary;
    this.link_url = link_url;
    this.rights = rights;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getImage_url() {
    return image_url;
  }

  public void setImage_url(String image_url) {
    this.image_url = image_url;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }

  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public String getLink_url() {
    return link_url;
  }

  public void setLink_url(String link_url) {
    this.link_url = link_url;
  }

  public String getRights() {
    return rights;
  }

  public void setRights(String rights) {
    this.rights = rights;
  }
}
