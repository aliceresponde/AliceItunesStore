package com.aliceresponde.itunesstore;

import android.app.Application;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by alice on 4/2/16.
 */
public class MyApplication extends Application {

  private RealmConfiguration realmConfiguration;
  private Retrofit retrofit;
  private ConnectivityManager connectivityManager;
  private NetworkInfo networkInfo;

  @Override public void onCreate() {
    super.onCreate();

    //Config REALM
    realmConfiguration = new RealmConfiguration.Builder((this)).build();

    //RetroFit
    retrofit = new Retrofit.Builder().baseUrl(getString(R.string.base_url_service))
        .addConverterFactory(GsonConverterFactory.create())
        .build();

    Realm.setDefaultConfiguration(realmConfiguration);

    //Internet
    connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
    networkInfo = connectivityManager.getActiveNetworkInfo();
  }

  public Retrofit getRetrofit() {
    return retrofit;
  }

  public boolean isOnline() {
    return networkInfo != null && networkInfo.isConnected();
  }
}
