package com.aliceresponde.itunesstore.Utilities;

import android.content.Context;
import com.aliceresponde.itunesstore.Entities.App;
import com.aliceresponde.itunesstore.Entities.Category;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by alice on 4/4/16.
 */
public class RealmUtilities {

  public static int countApps(Realm realm) {
    RealmQuery<App> query = realm.where(App.class);
    return (int) query.count();
  }

  public static int countCategories(Realm realm) {
    RealmQuery<Category> query = realm.where(Category.class);
    return (int) query.count();
  }

  public static RealmResults<App> getAllAppsQuery(Realm realm) {
    return realm.where(App.class).findAll();
  }

  public static RealmResults<Category> getAllCategoiesQuery(Realm realm) {
    return realm.where(Category.class).findAll();
  }

  public static RealmResults<App> getAppsByCategoryQuery(Realm realm, String category) {
    return realm.where(App.class).equalTo("category", category).findAll();
  }

  public static App findAppByName(Realm realm, String name){

    return  realm.where(App.class)
        .equalTo("name", name)
        .findFirst();
  }
}
