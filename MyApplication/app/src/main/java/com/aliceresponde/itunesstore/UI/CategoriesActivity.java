package com.aliceresponde.itunesstore.UI;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.aliceresponde.itunesstore.Adapters.IAppDetailListener;
import com.aliceresponde.itunesstore.DTO.Entry;
import com.aliceresponde.itunesstore.DTO.ServerResponse;
import com.aliceresponde.itunesstore.Entities.App;
import com.aliceresponde.itunesstore.Entities.Category;
import com.aliceresponde.itunesstore.MyApplication;
import com.aliceresponde.itunesstore.R;
import com.aliceresponde.itunesstore.Services.IService;
import com.aliceresponde.itunesstore.Utilities.RealmUtilities;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class CategoriesActivity extends AppCompatActivity {

  private Realm realm;
  @Bind(R.id.lv_categories) ListView lv_categories;
  private HashSet<String> categories;
  SimpleCursorAdapter mAdapter;
  private AppDetail dialog;




  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_categories);
    categories = new HashSet<String>();
    realm = Realm.getDefaultInstance();
    ButterKnife.bind(this);

    if (countApps()==20){
      countCategoriesAndLoad();
    }else if (isOnline()) {
      retreiveDataFromWS();
    }else {
      Toast.makeText(this, getString(R.string.no_internet),Toast.LENGTH_SHORT).show();
    }
  }

  private void retreiveDataFromWS() {
    IService service = getRetrofit().create(IService.class);
    Call<ServerResponse> responseCall = service.getAllApplications();
    responseCall.enqueue(new Callback<ServerResponse>() {
     @Override
     public void onResponse(Response<ServerResponse> response, Retrofit retrofit) {
         try {
               persistRequest(response);
             } catch (Exception e) {
               Log.e("error", "Saving data into REALM DB" + e.getMessage());
             }
         }

      @Override
      public void onFailure(Throwable t) {
          Log.e("Error", t.getMessage());
         }
      }
    );
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    realm.close();
  }

  private void persistRequest(Response<ServerResponse> response) {
    List<Entry> entries = new ArrayList<Entry>();
    entries = response.body().getFeed().getEntry();
    int i = 0;
    for (Entry entry : entries) {
      persistEntry(entry);
      String entry_category = entry.getCategory().getAttributes().getLabel();
      if (!categories.contains(entry_category)) {
        categories.add(entry_category);
        persistCategory(entry_category);
      }
    }

    countCategoriesAndLoad();

    realm.close();
  }

  private void persistCategory(String entry_category) {
    Category categoryItem = new Category(entry_category);
    realm.beginTransaction();
    realm.copyToRealm(categoryItem);
    realm.commitTransaction();
  }

  private void persistEntry(Entry entry) {
    String name = entry.getImName().getLabel();
    String category = entry.getCategory().getAttributes().getLabel();
    String image_url = entry.getImImage().get(2).getLabel();
    String summary = entry.getSummary().getLabel();
    String price = entry.getImPrice().getAttributes().getAmount();
    String link_url = entry.getLink().getAttributes().getHref();
    String rights = entry.getRights().getLabel();

    App appItem = new App(name, image_url, category, price, summary, link_url, rights);

    realm.beginTransaction();
    realm.copyToRealm(appItem);
    realm.commitTransaction();
  }

  private int countApps() {
    RealmQuery<App> query = realm.where(App.class);
    return (int) query.count();
  }

  private  void countCategoriesAndLoad(){
    RealmResults<Category> mCategories = RealmUtilities.getAllCategoiesQuery(realm);
    List<String> categories_array = new ArrayList<>();

    for (Category c: mCategories){
      categories_array.add(c.getName());
    }

    final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
        android.R.layout.simple_list_item_1, android.R.id.text1, categories_array);
    lv_categories.setAdapter(adapter);
    lv_categories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        goToGalery(position, adapter);
      }
    });


    Toast.makeText(CategoriesActivity.this, R.string.title_cache_working, Toast.LENGTH_SHORT).show();

  }

  private void goToGalery(int position, ArrayAdapter<String> adapter) {
    Intent intent = new Intent(CategoriesActivity.this, Galery.class);
    intent.putExtra(getString(R.string.key_category), adapter.getItem(position));
    startActivity(intent);
  }

  private boolean isOnline(){
    return ((MyApplication)getApplicationContext()).isOnline();
  }

  public Retrofit getRetrofit() {
    return ((MyApplication) getApplicationContext()).getRetrofit();
  }
}
