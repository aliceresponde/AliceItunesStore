package com.aliceresponde.itunesstore.UI;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.aliceresponde.itunesstore.Entities.App;
import com.aliceresponde.itunesstore.R;
import com.aliceresponde.itunesstore.Utilities.RealmUtilities;
import com.bumptech.glide.Glide;
import io.realm.Realm;

/**
 * Show application detail
 * Created by alice on 4/4/16.
 */
public class AppDetail extends AppCompatActivity {

  @Bind(R.id.imagev_app_icon) ImageView imagevAppIcon;
  @Bind(R.id.tv_app_name) TextView tvAppName;
  @Bind(R.id.tv_app_rigth) TextView tvAppRigth;
  @Bind(R.id.tv_app_category) TextView tvAppCategory;
  @Bind(R.id.btn_app_link) Button btnAppLink;
  @Bind(R.id.tv_app_summary) TextView tvAppSummary;

  private App app;
  private String name;
  private Realm realm;

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.app_detail);
    ButterKnife.bind(this);
    setTitle("Detail");

    realm = Realm.getDefaultInstance();
    name = getIntent().getExtras().getString("name");
    app = RealmUtilities.findAppByName(realm,name);

    Glide.with(imagevAppIcon.getContext())
        .load(app.getImage_url())
        .into(imagevAppIcon)
        .onLoadCleared(this.getResources().getDrawable(R.drawable.apple_apps_icon));

    tvAppName.setText(app.getName());
    tvAppCategory.setText(app.getCategory());
    tvAppRigth.setText(app.getRights());
    tvAppSummary.setText(app.getSummary());
  }

  @OnClick (R.id.btn_app_link)
  public void goToWeb(){
    Intent i = new Intent(Intent.ACTION_VIEW);
    i.setData(Uri.parse(app.getLink_url()));
    startActivity(i);

  }

  @Override protected void onDestroy() {
    super.onDestroy();
    realm.close();
  }
}
