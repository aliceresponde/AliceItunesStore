package com.aliceresponde.itunesstore.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.aliceresponde.itunesstore.Entities.App;
import com.aliceresponde.itunesstore.R;
import com.bumptech.glide.Glide;
import io.realm.RealmResults;

/**
 * Created by alice on 4/4/16.
 */

public class AppAdapter extends RecyclerView.Adapter<AppAdapter.AppHolder> {

  private IAppDetailListener mAppDetailListener;
  private LayoutInflater mInflater;
  private RealmResults<App> mResult;

  /**
   * You must  accept the listener
   */
  public AppAdapter(Context context, RealmResults<App> results, IAppDetailListener appDetailListener) {
    mInflater = LayoutInflater.from(context);
    mAppDetailListener = appDetailListener;
    update(results);
  }

  public void update(RealmResults<App> results) {
    mResult = results;
    notifyDataSetChanged();
  }

  public void setmResult(RealmResults<App> mResult) {
    this.mResult = mResult;
  }

  @Override public int getItemViewType(int position) {
    return super.getItemViewType(position);
  }

  @Override public AppHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = mInflater.inflate(R.layout.app_item, parent, false);
    AppHolder holder = new AppHolder(view, mAppDetailListener);
    return holder;
  }

  @Override public void onBindViewHolder(AppHolder holder, int position) {

    App app = mResult.get(position);
    Glide.with(holder.imagevAppIcon.getContext())
        .load(app.getImage_url())
        .into(holder.imagevAppIcon);

    holder.tvAppName.setText(app.getName());
    holder.tvAppRigth.setText(app.getRights());



  }

  @Override public int getItemCount() {
    return mResult.size();
  }

  /**
   * Class  responssible to manage every  item
   * Here the item is defined
   *
   * the holders root view  has assigned the onClickListent
   */
  public static class AppHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    @Bind(R.id.imagev_app_icon) ImageView imagevAppIcon;
    @Bind(R.id.tv_app_rigth) TextView tvAppRigth;
    @Bind(R.id.tv_app_name) TextView tvAppName;

    IAppDetailListener mAppDetailListener;

    public AppHolder(View itemView, IAppDetailListener listener) {
      super(itemView);
      itemView.setOnClickListener(this);
      mAppDetailListener = listener;
      ButterKnife.bind(this, itemView);
    }

    /**
     * Click event
     * Here is not possible to get access to Fragment manager or Activity
     * to lunch any activity, fragment, dialog..
     * The method to lunch it must to be inside the activity
     * example  showAppDetail()
     */
    @Override public void onClick(View v) {
      mAppDetailListener.onAppDetail(getAdapterPosition());
    }

    //@Override public void onClick(View v) {
    //
    //  App temApp = mResult.get(this.getAdapterPosition());
    //  Intent intent = new Intent(v.getContext(), AppDetail.class);
    //  intent.putExtra("name", temApp.getName());
    //  v.getContext().startActivity(intent);
    //}
  }
}
