package com.aliceresponde.itunesstore.Services;

import com.aliceresponde.itunesstore.DTO.ServerResponse;
import retrofit.Call;
import retrofit.http.GET;

/**
 * Created by alice on 4/2/16.
 * https://itunes.apple.com/us/rss/topfreeapplications/limit=20/json
 */
public interface IService {

    @GET("us/rss/topfreeapplications/limit=20/json")
    Call<ServerResponse> getAllApplications();

}
