package com.aliceresponde.itunesstore.UI;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.animation.OvershootInterpolator;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.aliceresponde.itunesstore.Adapters.AppAdapter;
import com.aliceresponde.itunesstore.Adapters.IAppDetailListener;
import com.aliceresponde.itunesstore.Entities.App;
import com.aliceresponde.itunesstore.MyApplication;
import com.aliceresponde.itunesstore.R;

import com.aliceresponde.itunesstore.Utilities.RealmUtilities;
import com.aliceresponde.itunesstore.Utilities.ScreenOrientationUtility;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.internal.Util;
import it.gmariotti.recyclerview.adapter.AlphaAnimatorAdapter;
import it.gmariotti.recyclerview.itemanimator.SlideInOutLeftItemAnimator;
import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.FlipInTopXAnimator;
import jp.wasabeef.recyclerview.animators.SlideInDownAnimator;

public class Galery extends AppCompatActivity {

  @Bind(R.id.recyclerv_galery_content) RecyclerView recyclervGaleryContent;

  private Realm realm;
  private RealmResults<App> mResults;
  private AppAdapter adapter;
  private IAppDetailListener mAppDetailListener= new IAppDetailListener() {
    @Override public void onAppDetail(int position) {
      goToDetail(position);
    }
  };

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_galery);
    ButterKnife.bind(this);

    String category = (String) getIntent().getExtras().get(getString(R.string.key_category));
    setTitle(category);
    realm = Realm.getDefaultInstance();
    mResults = RealmUtilities.getAppsByCategoryQuery(realm, category);

    LinearLayoutManager managerList = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL, false);
    GridLayoutManager managerGrid = new GridLayoutManager(this,4);

    //recyclervGaleryContent.setLayoutManager(managerList);

    defineDeviseOrientation(managerList, managerGrid);
    setCoolAnimation();
  }

  private void setCoolAnimation() {
    adapter = new AppAdapter(this, mResults , mAppDetailListener);
    AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(adapter);
    alphaAdapter.setInterpolator(new OvershootInterpolator(1f));
    recyclervGaleryContent.setAdapter(new ScaleInAnimationAdapter(alphaAdapter));
    recyclervGaleryContent.getItemAnimator().setAddDuration(2000);
    recyclervGaleryContent.getItemAnimator().setRemoveDuration(2000);
    recyclervGaleryContent.getItemAnimator().setMoveDuration(2000);
    recyclervGaleryContent.getItemAnimator().setChangeDuration(2000);
  }

  private void defineDeviseOrientation(LinearLayoutManager managerList,
      GridLayoutManager managerGrid) {
    recyclervGaleryContent.setHasFixedSize(true);
    if (ScreenOrientationUtility.isTablet(this)){
      Log.d("devise", "Tablet");
      recyclervGaleryContent.setLayoutManager(managerGrid);
    }else{
      recyclervGaleryContent.setLayoutManager(managerList);
      Log.d("devise", "Celu");
    }

    ScreenOrientationUtility.defineOrientationScreen(this);
  }

  private void goToDetail(int position) {

    Log.d("oToDetail",mResults.get(position).getName());
    Intent intent = new Intent(this, AppDetail.class);
    intent.putExtra("name",mResults.get(position).getName());
    startActivity(intent);
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    realm.close();
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.categories, menu);
    return true;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {

    int option = item.getItemId();

    switch (option) {
      case R.id.action_all_categories:
        synchronized (Galery.this) {
          mResults = RealmUtilities.getAllAppsQuery(realm);
          adapter.setmResult(mResults);
        }
        adapter.notifyDataSetChanged();
        setTitle("All");
        break;

      case R.id.action_categpries:
        finish();
        break;
    }

    return true;
  }

  private boolean isOnline() {
    return ((MyApplication) getApplicationContext()).isOnline();
  }
}