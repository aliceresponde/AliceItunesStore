package com.aliceresponde.itunesstore.Entities;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by alice on 4/4/16.
 */
public class Category extends RealmObject {
  @PrimaryKey
  private String name ;

  public Category() {
  }

  public Category(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
