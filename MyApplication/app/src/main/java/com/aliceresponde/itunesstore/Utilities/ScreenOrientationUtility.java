package com.aliceresponde.itunesstore.Utilities;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.util.Log;

/**
 * Created by alice on 4/4/16.
 */
public class ScreenOrientationUtility {


    public static  boolean isTablet(Context context) {
      boolean xlarge = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_XLARGE);
      boolean large = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
      return (xlarge || large);
    }


  public  static  int getScreenSize(Context context){
    return context.getResources().getConfiguration()
        .screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;
  }


  /**
   * Check size and change screen orientation
   * @param activity
   */
  public static void defineOrientationScreen(Activity activity) {
    if (isTablet(activity)) {
      activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    } else activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
  }
}
