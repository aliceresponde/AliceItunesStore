package com.aliceresponde.itunesstore.Adapters;

/**
 * Created by alice on 4/5/16.
 * way  to communicate method showAppDetail()  between activity and AppAdapter
 */
public interface IAppDetailListener {
  /**
   * Because when  you click a position of recyclerV, you will see AppDetails
   * @param position
   */
  void onAppDetail(int position);
}
