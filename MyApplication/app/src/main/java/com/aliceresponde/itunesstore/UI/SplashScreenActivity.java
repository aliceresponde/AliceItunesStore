package com.aliceresponde.itunesstore.UI;

import android.content.Intent;
import android.graphics.PixelFormat;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Window;

import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import butterknife.Bind;
import butterknife.ButterKnife;
import com.aliceresponde.itunesstore.MyApplication;
import com.aliceresponde.itunesstore.R;
import com.aliceresponde.itunesstore.Utilities.RealmUtilities;
import io.realm.Realm;

public class SplashScreenActivity extends AppCompatActivity {

  @Bind (R.id.splash)  ImageView imageViewIcon;

  Thread splashTread;
  private Realm realm;
  private int storedApp=0;

  public void onAttachedToWindow() {
    super.onAttachedToWindow();
    Window window = getWindow();
    window.setFormat(PixelFormat.RGBA_8888);
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_splash);
    ButterKnife.bind(this);

    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    realm = Realm.getDefaultInstance();
    storedApp =RealmUtilities.countApps(realm);
    setSupportActionBar(toolbar);

    startAnimations();
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    realm.close();
  }

  private void startAnimations() {
    Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
    anim.reset();
    LinearLayout l=(LinearLayout) findViewById(R.id.lin_lay);
    l.clearAnimation();
    l.startAnimation(anim);

    anim = AnimationUtils.loadAnimation(this, R.anim.translate);
    anim.reset();


    splashTread = new Thread() {
      @Override
      public void run() {
        try {
          int waited = 0;
          // SplashScreenActivity screen pause time
          while (waited < 3500) {
            sleep(100);
            waited += 100;
          }


          ConnectivityManager connectivityManager =(ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
          NetworkInfo networkInfo= connectivityManager.getActiveNetworkInfo();

          Log.e("nternet", isOnline()+"");

          if (isOnline() || storedApp!=0){
            goToCategories();
          }else{
            goToNotInternet();
          }

        } catch (InterruptedException e) {
          // do nothing
        } finally {
          SplashScreenActivity.this.finish();
        }

      }
    };
    splashTread.start();

  }

  /**
   * No internet , not data in cache
   */
  private void goToNotInternet() {
    Intent intent = new Intent(SplashScreenActivity.this, NotInternetConnection.class);
    startActivity(intent);
  }

  private boolean isOnline(){
    return  ((MyApplication) getApplicationContext()).isOnline();
  }

  private void goToCategories() {
    Intent intent = new Intent(SplashScreenActivity.this, CategoriesActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
    startActivity(intent);
    SplashScreenActivity.this.finish();
  }
}

