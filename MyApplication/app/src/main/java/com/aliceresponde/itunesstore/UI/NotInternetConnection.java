package com.aliceresponde.itunesstore.UI;

import android.content.Intent;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.aliceresponde.itunesstore.R;

public class NotInternetConnection extends AppCompatActivity {

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_not_internet_connection);
  }

  @Override public void onBackPressed() {
    super.onBackPressed();
    Intent intent = new Intent(NotInternetConnection.this, SplashScreenActivity.class);
    startActivity(intent);
  }
}
